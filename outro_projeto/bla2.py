# NEBULUZ AIRFLOW DAG

#Library
from datetime import timedelta
import airflow
import psycopg2
import pandas as pd
#from sqlalchemy import create_engine #connect pandas to postgresql
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.python_operator import BranchPythonOperator

#IMPORTING PYTHON SCRIPTS


# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'IHM Stefanini',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(1),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'adhoc':False,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'trigger_rule': u'all_success'
}

dag = DAG(
    'Nebuluz_Mono',
    catchup = False,
    default_args=default_args,
    description='IHM Stefanini Specialist System',
    schedule_interval=timedelta(minutes=1),
)
# GLOBAL VARIABLES
host = 'localhost' #Defening host
database = 'nblzcbmm' #DB name
user = 'postgres' #DB user
password = '1234' #DB password
#conexão com banco de dados
#con=Conexao(host,database,user,password)

#FLOW VARIABLES DICTIONARY
flow_variables = {} #Flow Variables dictionary used by python scripts


def nebuluz():
   psycopg2.connect(host=mhost, database=db, user=usr,  password=pwd)

#Connect and read from DB function
def conectar():
    print("Iniciando leitura:")
    sql = """SELECT a."Timestamp", b."InTag", a."Value", b."Type" FROM public."InTagValue" AS a JOIN public."InTag" AS b ON a."InTag_Id" = b."Id" WHERE b."Workflow" = 1 ORDER BY "Timestamp" DESC LIMIT 30600"""
    rs=con.consultar(sql)
    #con.fechar()
    print("Consulta ao banco de dados realizada com sucesso")
    return(rs) #return and send to xcom

#Data Validation Function
#Branchs into the correct task checking if data is valid or not
def data_validation (input_table):
    #ti = kwargs['ti']
    #input_table = ti.xcom_pull(task_ids='DatabaseReader') # Read the table from xcom DatabaseReader task
    output = validation(input_table, flow_variables) #call validation function
    print("validação dos dados realizada com sucesso")
    #kwargs['ti'].xcom_push(key='validation_table', value=output)
    #kwargs['ti'].xcom_push(key='flow', value=flow_variables)
    #Check data validation
    return(output,flow_variables)
 

#Pre process data
def pre_processing (input_table1,flow_variables):
    #ti = kwargs['ti']
    #input_table1 = ti.xcom_pull(key='validation_table', task_ids='DataValidation')
    #flow_variables = ti.xcom_pull(key='flow', task_ids='DataValidation')
    #Check data validation
    print("Pre processando dados")
    output = pre_processing_data(input_table1,flow_variables) #pre process valid data
    print("Pre processamento realizado com sucesso")
    #kwargs['ti'].xcom_push(key='flow', value=flow_variables)
    return(output,flow_variables)

def process_invalid_data (input_table1,flow_variables):
    #ti = kwargs['ti']
    #con=Conexao(host,database,user,password)#Connect to DB
    #input_table1 = ti.xcom_pull(key='validation_table', task_ids='DataValidation')
    #flow_variables = ti.xcom_pull(key='flow', task_ids='DataValidation')
    sql = """SELECT "Timestamp", "OutTag", "Value"  FROM "OutTagValue" WHERE "Timestamp" = (SELECT MAX("Timestamp") FROM "OutTagValue") AND "OutTag" LIKE '%CICLO%'"""
    input_table2 = con.consultar(sql) #Read table
    if input_table2.shape[0] == 0:
        print("DataFrame Vazio!!")
    #con.fechar()
    print("Leitura no Banco de Dados com sucesso")
    table_output,tags_dictionary = invalid(input_table1,input_table2,flow_variables) #Treat the invalid data
    rabbit_producer(tags_dictionary) #OPC write
    print("Tags enviadas para o Rabbit")
    #engine = create_engine('postgresql+psycopg2://postgres:1234@localhost:5432/dvdrental')#Connect to Write Pandas DataFrame
    #table_output.to_sql('OutTagValue',engine,if_exists='append',index = False)#DataBase Write
    sql = """INSERT INTO "OutTagValue1" ("Timestamp", "OutTag", "Value") VALUES (%s, %s, %s)"""
    check=con.manipular(sql = sql,data = table_output)
    if check:
        print("Escrita no Banco de Dados com sucesso")
    else:
        print("Falha na escrita!")
    con.fechar()#Close DB conection

#Apply fuzzt rules
def fuzzy (input_table2,flow_variables):
    #ti = kwargs['ti']
    #flow_variables = ti.xcom_pull(key='flow', task_ids='PreProcessing')
    #con=Conexao(host,database,user,password) #connecting to DB
    #print("Conexao com banco de dados realizada com sucesso")
    sql = """SELECT "Timestamp", "OutTag", "Value" FROM "OutTagValue" WHERE ("OutTag" LIKE '%CICLO%' OR "OutTag" LIKE '%CONTADOR%') ORDER BY "Timestamp" DESC LIMIT 18"""
    input_table1 = con.consultar(sql) #result from query
    if input_table1.shape[0] == 0:
            print("DataFrame Vazio!!")
    #con.fechar()
    print("Consulta ao banco de dados realizada com sucesso")
    #input_table2 = ti.xcom_pull(task_ids='PreProcessing') #getting table from xcom
    output1, output2 = apply_rules(input_table1, input_table2,flow_variables) #applying fuzzy rules
    print("Regras Fuzzy aplicadas com sucesso")
    #kwargs['ti'].xcom_push(key='flow', value=flow_variables)
    return(output1, output2,flow_variables)
    

#process data after applying fuzzy rules
def post_processing (input_table1,input_table2,flow_variables):
    #ti = kwargs['ti']
    #input_table1, input_table2 = ti.xcom_pull(task_ids='FuzzyRules') # Read the table from xcom FuzzyRules task
    #flow_variables = ti.xcom_pull(key='flow', task_ids='FuzzyRules')
    output1, output2 = post_processing_data(input_table1, input_table2, flow_variables) #post processing valid data
    print("Pós processamento realizado com sucesso")
    #kwargs['ti'].xcom_push(key='flow', value=flow_variables)
    return(output1, output2,flow_variables)

#Function to write the data in Postgresql and PLC, after post processing 
def write (input_table1,input_table2,flow_variables):
    #ti = kwargs['ti']
    #input_table1, input_table2 = ti.xcom_pull(task_ids='PostProcessing') #Read xcom tables from PostProcessing task
    #flow_variables = ti.xcom_pull(key='flow', task_ids='PostProcessing')
    sql = """INSERT INTO "OutTagValue1" ("Timestamp", "OutTag", "Value") VALUES (%s, %s, %s)"""
    check=con.manipular(sql = sql,data = input_table1)
    if check:
        print("Escrita no Banco de Dados com sucesso")
    else:
        print("Falha na escrita no banco de dados!")
    print("Escrita no Banco de Dados realizada com sucesso")
    tags_dictionary = define_valid_tags(input_table2,flow_variables) #Define tags to OPC write
    rabbit_producer(tags_dictionary) #Send tags by rabbitmq
    print("tags",tags_dictionary)
    print("Tags enviadas para o Rabbit")
    con.fechar() #Close DB conection

# Defining airflow tasks

t1 = PythonOperator(
     task_id='Nebuluz_Task',
     provide_context=False,
     python_callable=nebuluz,
     dag=dag,
)



#Task Dependency
t1 