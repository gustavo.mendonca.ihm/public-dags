from datetime import timedelta
import airflow
import psycopg2
import pandas as pd
#from sqlalchemy import create_engine #connect pandas to postgresql
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.python_operator import BranchPythonOperator


#FLOW VARIABLES DICTIONARY
flow_variables = {} #Flow Variables dictionary used by python scripts

#IMPORTING PYTHON SCRIPTS
import sys
sys.path.insert(0,r'/usr/local/airflow/scripts/Loop') #path to python scripts
#from oMO_KPIs_v06 import calcula_kpis_raw


default_args = {
    'owner': 'IHM Stefanini',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(1),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
}

dag = DAG(
        'Kpi_Executor_V01c',
        catchup = False,
        default_args=default_args,
        description='IHM Stefanini Loop',
        schedule_interval=timedelta(minutes=10),
        )


###################################################################################################
##
##
##
###################################################################################################

def get_loop_data(**kwargs):
    malha = 100
    return malha

def define_loop_dataset(**kwargs):
    set_of_malhas = 100
    return set_of_malhas


def calc_loop_kpi(**kwargs):
    set_of_malhas = 100
    return set_of_malhas


def output_loop_kpi(**kwargs):
    set_of_malhas = 100
    return set_of_malhas

def process_invalid_data(**kwargs):
    set_of_malhas = 100
    return set_of_malhas


###################################################################################################
##
##
##
###################################################################################################

# Defining airflow tasks
t1 = PythonOperator(
    task_id='Load_Loop_Data',
    provide_context=False,
    python_callable=get_loop_data,
    dag=dag,
)
t2 = BranchPythonOperator(
     task_id='Define_Loop_Dataset',
     provide_context=True,
     python_callable=define_loop_dataset,
     dag=dag,
)

t3 = PythonOperator(
     task_id='Calc_Loop_Kpi_01',
     provide_context=True,
     python_callable=calc_loop_kpi,
     dag=dag,
)

t4 = PythonOperator(
     task_id='Calc_Loop_Kpi_02',
     provide_context=True,
     python_callable=calc_loop_kpi,
     dag=dag,
)

t5 = PythonOperator(
     task_id='Calc_Loop_Kpi_03',
     provide_context=True,
     python_callable=calc_loop_kpi,
     dag=dag,
)

t6 = PythonOperator(
     task_id='PostProcessing',
     provide_context=True,
     python_callable=output_loop_kpi,
     dag=dag,
)


tinvalid = PythonOperator(
     task_id='ProcessingInvalidData',
     provide_context=True,
     python_callable=process_invalid_data,
     dag=dag,
)

#Tasks Dependency
t1 >> t2 
t2 >> t3
t2 >> t4
t2 >> t5
t3 >> t6
t4 >> t6
t5 >> t6
t2 >> tinvalid